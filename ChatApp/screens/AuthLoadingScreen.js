import React from 'react';
import{
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import firebase from 'firebase';
import User from '../User';

export default class AuthLoadingScreen extends React.Component{
    constructor(props){
        super(props);
        this._bootstrapAsync();
    }

    UNSAFE_componentWillMount(){
        var firebaseConfig = {
            apiKey: "AIzaSyAbIIM0o3Nn9XsdyKo88UNjNXTHQqqDf4o",
            authDomain: "chatapp-7e673.firebaseapp.com",
            projectId: "chatapp-7e673",
            storageBucket: "chatapp-7e673.appspot.com",
            messagingSenderId: "269088349240",
            appId: "1:269088349240:web:d0b3d72c3f176e2c239acd",
            measurementId: "G-NY6XW0DEHP",
            databaseURL: "https://chatapp-7e673-default-rtdb.firebaseio.com/"
        };

        firebase.initializeApp(firebaseConfig);
    }


    _bootstrapAsync = async () => {
        User.phone = await AsyncStorage.getItem("userPhone");



        this.props.navigation.navigate(User.phone ? 'App' : 'Auth');
    };


    render(){
        return(
            <View>
                <ActivityIndicator />
                    <StatusBar barStyle="default"></StatusBar>
            </View>
        );
    }
}